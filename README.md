# AAA analysis

## Description

This repository contains code for segmenting Abdominal Aortic Aneurysms and calculating their max diameters.

## Usage

### Segmentation

MATLAB code for segmentation is found in the "segmentation" folder.  The segmentation model is described in [this paper](https://doi.org/10.1016/j.media.2017.05.005)

For more information on how to use this code, contact the original author: yan.wang2@ucsf.edu.

### Max Diameter calculation

The script file is named "main.py".  Add ".../aaa_analysis" to PYTHONPATH and modify the parameters in main.py, then run the script either from the command line or in an IDE.

## Requirements

All requirements should be installable through Anaconda.  Check the VMTK website for more information.

- VMTK (VTK, numpy) >= 1.4
- scipy

## Docs and tests

Sadly there are none right now.  To request more documentation, please open a new issue.
