# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 11:38:58 2020

@author: tossi
"""

from collections import OrderedDict

import numpy as np

import vtk
from vtk.numpy_interface import dataset_adapter as dsa

wdo = dsa.WrapDataObject

from vmtk import vmtkscripts

# data i/o
def set_vtk_error_output_file(filepath):
    errOut = vtk.vtkFileOutputWindow()
    errOut.SetFileName(filepath)
    vtkStdErrOut = vtk.vtkOutputWindow()
    vtkStdErrOut.SetInstance(errOut)
    return errOut, vtkStdErrOut

def read_vtkxml(filepath):
    if filepath.endswith('.vts'):
        reader = vtk.vtkXMLStructuredGridReader()
    elif filepath.endswith('.vtu'):
        reader = vtk.vtkXMLUnstructuredGridReader()
    elif filepath.endswith('.vti'):
        reader = vtk.vtkXMLImageDataReader()
    elif filepath.endswith('.vtp'):
        reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(filepath)
    reader.Update()
    return reader.GetOutput()

def save_vtkxml(dataset, filepath):
    if isinstance(dataset, vtk.vtkStructuredGrid):
        writer = vtk.vtkXMLStructuredGridWriter()
    elif isinstance(dataset, vtk.vtkImageData):
        writer = vtk.vtkXMLImageDataWriter()
    elif isinstance(dataset, vtk.vtkPolyData):
        writer = vtk.vtkXMLPolyDataWriter()
    elif isinstance(dataset, vtk.vtkUnstructuredGrid):
        writer = vtk.vtkXMLUnstructuredGridWriter()
    writer.SetInputData(dataset)
    writer.SetFileName(filepath)
    writer.Update()

# vtk/numpy interface
def reshape_ND_to_2D_array(array):

    numTuples = np.prod(array.shape[:3])
    if array.ndim == 3:
        numComponents = 1
        array = array[..., np.newaxis]
    elif array.ndim == 4:
        numComponents = array.shape[-1]
    elif array.ndim == 7:
        numComponents = array.shape[-1]
    else:
        raise Exception(
            'Cannot process arrays of {0}-dim, only 3-4 dims'.format(array.ndim))

    arrayReshaped = np.zeros((numTuples, numComponents))
    for j in range(numComponents):
        arrayReshaped[:, j] = array[..., j].ravel(order='F')

    return arrayReshaped.squeeze()

def numpy_array_to_vtk_image(array, name, spacing):
    
    image = vtk.vtkImageData()
    image.SetSpacing(spacing)
    image.SetDimensions(array.shape)
    
    arrayReshaped = reshape_ND_to_2D_array(array)
    
    image_dsa = wdo(image)
    image_dsa.PointData.append(arrayReshaped, name)
    image.GetPointData().SetActiveScalars(name)
    return image

# vmtk functions
def smooth_image(image, method='gauss', standardDeviation=1.0,
                 radiusFactor=5.0, dimensionality=3,
                 timeStep=0.0625, autoCalculateTimeStep=1,
                 conductance=1.0, numberOfIterations=5):
    
    imageSmoother = vmtkscripts.vmtkImageSmoothing()

    imageSmoother.Image = image
    imageSmoother.Method = method
    
    if method == 'gauss':
        imageSmoother.StandardDeviation = standardDeviation # pixel
        imageSmoother.RadiusFactor = radiusFactor
        imageSmoother.Dimensionality = dimensionality
    elif method == 'anisotropic':
        imageSmoother.NumberOfIterations = numberOfIterations
        if not autoCalculateTimeStep:
            imageSmoother.TimeStep = timeStep
        imageSmoother.Conductance = conductance
        
    imageSmoother.Execute()
    return imageSmoother.Image

def marching_cubes(image, level):
    marchingCubes = vmtkscripts.vmtkMarchingCubes()
    marchingCubes.Image = image
    marchingCubes.Level = level
    marchingCubes.Execute()
    
    cleaner = vtk.vtkCleanPolyData()
    cleaner.SetInputData(marchingCubes.Surface)
    cleaner.Update()
    
    return cleaner.GetOutput()

def smooth_surface(surface, numIterations=30, passBand=0.01):
    surfaceSmoother = vmtkscripts.vmtkSurfaceSmoothing()
    surfaceSmoother.Surface = surface
    surfaceSmoother.NumberOfIterations = numIterations
    surfaceSmoother.PassBand = passBand
    surfaceSmoother.Execute()
    return surfaceSmoother.Surface

def cap_surface(surface):
    capper = vmtkscripts.vmtkSurfaceCapper()
    capper.Surface = surface
    capper.Interactive = 0
    capper.Execute()
    return capper.Surface

def pad_image_by_one(image):
    extent = np.array(image.GetExtent())
    extent[::2] -= 1
    extent[1::2] += 1
    
    constant = image.GetPointData().GetScalars().GetRange()[0]
    
    padder = vtk.vtkImageConstantPad()
    padder.SetInputData(image)
    padder.SetOutputWholeExtent(extent)
    padder.SetConstant(constant)
    padder.Update()
    
    return padder.GetOutput()


# visualization
def init_surface_viewer(surface, vmtkRenderer=None, opacity=1.0, 
                        arrayName='', scalarRange=[0.0,0.0],
                        colorMap='cooltowarm', numberOfColors=256,
                        legend=0, legendTitle='', grayscale=0, 
                        flatInterpolation=0, displayCellData=0,
                        color=[-1.0,-1.0,-1.0], display=0,
                        execute=True, **kwargs):

    viewer = vmtkscripts.vmtkSurfaceViewer()
    if vmtkRenderer:
        viewer.vmtkRenderer = vmtkRenderer
    
    viewer.Surface = surface

    viewer.Opacity = opacity
    viewer.ArrayName = arrayName
    viewer.ScalarRange = scalarRange
    viewer.ColorMap = colorMap
    viewer.NumberOfColors = numberOfColors
    viewer.Legend = legend
    viewer.LegendTitle = legendTitle
    viewer.Grayscale = grayscale
    viewer.FlatInterpolation = flatInterpolation
    viewer.DisplayCellData = displayCellData
    viewer.Color = color
    viewer.Display = display

    for k,v in kwargs.items():
        try:
            viewer.__setattr__(k, v)
        except:
            print('Viewer has no attribute {0}.'.format(k))
    
    if execute:
        viewer.Execute()        
    
    return viewer

def view_surface(surface, settings=OrderedDict()):
    settings['display'] = 1
    init_surface_viewer(surface, **settings)

def view_datasets(datasets: list, dataSetSettings: list):
    
    ren = vmtkscripts.vmtkRenderer()
    ren.Execute()
    
    viewers = []
    for dataset, settings in zip(datasets, dataSetSettings):
        settings['vmtkRenderer'] = ren
        if dataset == datasets[-1]:
            settings['display'] = 1
        viewer = init_surface_viewer(dataset, **settings)
        viewers.append(viewer)
        
    del ren.RenderWindow, ren.RenderWindowInteractor  