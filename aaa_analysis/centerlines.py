# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 11:22:15 2020

@author: tossi
"""

from vmtk import vmtkscripts

# --- helper functions ---
def capitalize_first_letter(string):
    return f'{string[0].upper()}{string[1:]}'

def set_vmtk_options(vmtkPypesObject, options):
    
    doNotCapitalize = ['vmtkRenderer']
    className = vmtkPypesObject.__class__.__name__
    
    for inputName, inputArg in options.items():
        if inputName not in doNotCapitalize:
            inputName = capitalize_first_letter(inputName)

        if inputName in vmtkPypesObject.__dict__:
            vmtkPypesObject.__dict__[inputName] = inputArg
        else:
            print(f'{className} has no attribute {inputName}.')

# --- centerline functions ---
def get_centerlines(surface, seedSelectorName='pickpoint', appendEndPoints=False,
                **kwargs):
    """
    Parameters
    -----
    surface: vtkPolyData

    Optional Parameters
    -----
    appendEndPoints: bool = False
        Appends endpoints to centerlines.
    seedSelector: vmtk.vmtkcenterlines.vmtkSeedSelector = None
        A vmtkSeedSelector object that specifies how the source and target
        seeds are selected.
    seedSelectorName: {'pickpoint','openprofiles','carotidprofiles',
                       'idlist','pointlist','profileidlist'} = 'pickpoint'
        Name of the method used to determine seeds, if seedSelector not given.
    flipNormals: bool = False
    capDisplacement: float = 0.0
    checkNonManifold: bool = False
        If True, will perform vmtkNonManifoldSurfaceChecker on surface.
    
    
    Returns
    -----
    centerlines: vtkPolyData
    """    
    centerliner = vmtkscripts.vmtkCenterlines()
    centerliner.Surface = surface
    centerliner.SeedSelectorName = seedSelectorName
    centerliner.AppendEndPoints = appendEndPoints
    
    set_vmtk_options(centerliner, kwargs)
    
    centerliner.Execute()
    return centerliner.Centerlines
            
def process_centerlines(centerline, length=.001): 
    """ Performs multiple centerline operations, including:
        * Smoothing the centerline
        * Resampling the centerline
        * Getting geometric information like the maximum inscribed sphere 
          radius or curvature
        * Getting the attribute information, like the abscissas and parallel
          transport normals
    """
    centerline = smooth_centerline(centerline)
    centerline = resample_centerline(centerline, length)
    centerline = get_centerline_geometry(centerline)
    centerline = get_centerline_attributes(centerline)
    return centerline

def get_centerline_attributes(centerlines, **kwargs):
    attributer = vmtkscripts.vmtkCenterlineAttributes()
    attributer.Centerlines = centerlines
    set_vmtk_options(attributer, kwargs)
    attributer.Execute()
    return attributer.Centerlines

def get_centerline_geometry(centerlines, **kwargs):
    geometer = vmtkscripts.vmtkCenterlineGeometry()
    geometer.Centerlines = centerlines
    set_vmtk_options(geometer, kwargs)
    geometer.Execute()
    return geometer.Centerlines

def resample_centerline(centerlines, length):
    resampler = vmtkscripts.vmtkCenterlineResampling()
    resampler.Centerlines = centerlines
    resampler.Length = length
    resampler.Execute()
    return resampler.Centerlines

def smooth_centerline(centerlines, numberOfSmoothingIterations=100,
                         smoothingFactor=0.1, **kwargs):
    
    centerlineSmoothing = vmtkscripts.vmtkCenterlineSmoothing()
    centerlineSmoothing.Centerlines = centerlines
    centerlineSmoothing.NumberOfSmoothingIterations = numberOfSmoothingIterations
    centerlineSmoothing.SmoothingFactor = smoothingFactor
    
    set_vmtk_options(centerlineSmoothing, kwargs)
    
    centerlineSmoothing.Execute()
    
    return centerlineSmoothing.Centerlines