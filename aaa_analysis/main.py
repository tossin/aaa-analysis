# -*- coding: utf-8 -*-
"""
Created on Tue May 21 23:09:00 2019

@author: Evan Kao
"""

import os
import functools
from collections import OrderedDict

import vtk
import numpy as np
import scipy.io

from aaa_analysis import (set_vtk_error_output_file,
                          numpy_array_to_vtk_image,
                          smooth_image,
                          pad_image_by_one,
                          marching_cubes,
                          smooth_surface,
                          read_vtkxml,
                          save_vtkxml,
                          get_centerlines,
                          process_centerlines,
                          SurfaceMaxDiameterExtraction,
                          view_datasets)

#%% Parameters
# filenames
wd = r'D:\UserData\Fei\MRI'
matfilename = 'result-chextend-bbpre-011.mat'
centerlinesFileName = ''
suffix = ''
vtkerrorlog = 'vtkerror.log'

# matlab array names
useOuterWallForCenterlines = True
spacingNames = ('solutionx','solutiony','solutionz')
imageArrayName = 'Img'
outerWallArrayName = 'res_outerf'
innerWallArrayName = 'res_innerf'
if 'bbpre' in matfilename:
    spacingOrder = (1,2,0)
    imageType = 'BlackBlood'
else:
    spacingOrder = (0,1,2)
    imageType = 'CTA'

# image smoothing parameters
imageSmoothing = True
imageSmoothingStandardDeviation = 2.0

# marching cubes parameters
level = 0.5

# surface smoothing parameters
surfaceSmoothing = False
numberOfSmoothingIterations = 30
smoothingPassBand = 0.1

# max diameter measurement parameters
centerlinesResamplingLength = 1 
appendEndPoints = False
normalsArrayName = 'FrenetTangent'
zeroDegreeArrayName = 'ParallelTransportNormals'
startIndex = 5
endIndex =-0
interval = 1
pointsPerSection = 360
outputMaxDiameterLines = True

#%%
if __name__ == '__main__':

    prepend_wd = functools.partial(os.path.join, wd)
    err, stderr = set_vtk_error_output_file(prepend_wd(vtkerrorlog))
        
    matfilepath = prepend_wd(matfilename)
    
    matlabData = scipy.io.loadmat(matfilepath)
    
    print('Arrays in MATLAB file:')
    for k in matlabData.keys():
        print(k)
    
    spacing = np.array([matlabData[spacingNames[i]].squeeze() for i in spacingOrder])
    print(f'Image spacing: {spacing}')
    #%%
    # convert inner wall segmentation into a polygonal surface
    innerWallSeg = matlabData[innerWallArrayName]
    innerWallSegImage = numpy_array_to_vtk_image(innerWallSeg, 'InnerWall', spacing)
    
    if imageSmoothing:
        innerWallSegImageSmoothed = smooth_image(innerWallSegImage, method='gauss',
            standardDeviation=imageSmoothingStandardDeviation)
    else:
        innerWallSegImageSmoothed = innerWallSegImage
    
    innerWallSegImage = pad_image_by_one(innerWallSegImage)
    innerWallSegImageSmoothed = pad_image_by_one(innerWallSegImageSmoothed)
    innerWallSurface = marching_cubes(innerWallSegImageSmoothed, level)
    
    if surfaceSmoothing:
        innerWallSurfaceSmoothed = smooth_surface(innerWallSurface,
                                                  numIterations = numberOfSmoothingIterations,
                                                  passBand = smoothingPassBand)
    else:
        innerWallSurfaceSmoothed = innerWallSurface
    
    #%%
    # convert outer wall segmentation into a polygonal surface
    outerWallSeg = matlabData[outerWallArrayName]
    outerWallSegImage = numpy_array_to_vtk_image(outerWallSeg, 'OuterWall', spacing)
    
    if imageSmoothing:
        outerWallSegImageSmoothed = smooth_image(outerWallSegImage, method='gauss',
            standardDeviation=imageSmoothingStandardDeviation)
    else:
        outerWallSegImageSmoothed = outerWallSegImage
    
    outerWallSegImage = pad_image_by_one(outerWallSegImage)
    outerWallSegImageSmoothed = pad_image_by_one(outerWallSegImageSmoothed)
    outerWallSurface = marching_cubes(outerWallSegImageSmoothed, level)
    
    if surfaceSmoothing:
        outerWallSurfaceSmoothed = smooth_surface(outerWallSurface,
                                                  numIterations = numberOfSmoothingIterations,
                                                  passBand = smoothingPassBand)
    else:
        outerWallSurfaceSmoothed = outerWallSurface
    
    #%%
    # extract centerlines from inner wall surface
    if centerlinesFileName:
        centerlines = read_vtkxml(prepend_wd(centerlinesFileName))
        centerlinesImported = True
    else:
        if useOuterWallForCenterlines:
            centerlinesSurface = outerWallSurfaceSmoothed
        else:
            centerlinesSurface = innerWallSurfaceSmoothed
        centerlines = get_centerlines(centerlinesSurface, appendEndPoints=int(appendEndPoints))
        centerlines = process_centerlines(centerlines, 
                                                  length=centerlinesResamplingLength)
        centerlinesImported = False
    
    # calculate the maximum diameters 
    maxDiameterExtractor = SurfaceMaxDiameterExtraction(
            outerWallSurfaceSmoothed, 
            centerlines,
            normalsArrayName,
            zeroDegreeArrayName,
            startIndex=startIndex,
            endIndex=endIndex,
            interval=interval,
            pointsPerSection=pointsPerSection,
            outputMaxDiameterLines=outputMaxDiameterLines)
    
    crossSections = maxDiameterExtractor()
    
    surfaceViewSettings = OrderedDict(
            opacity=0.2)    
    crossSectionsViewSettings = OrderedDict()
    centerlineViewSettings = OrderedDict(
            arrayName='MaximumInscribedSphereRadius',
            colorMap='rainbow')
    
    datasets = [outerWallSurfaceSmoothed, crossSections, centerlines]
    settings = [surfaceViewSettings, crossSectionsViewSettings,
                centerlineViewSettings]
    
    view_datasets(datasets, settings)
    #%% group all created datasets
    allDataSets = OrderedDict(
            inner_wall_surface=innerWallSurface,
            outer_wall_surface=outerWallSurface,
            cross_sections=crossSections
            )
    
    if surfaceSmoothing:
        allDataSets['inner_wall_surface_smoothed'] = innerWallSurfaceSmoothed
        allDataSets['outer_wall_surface_smoothed'] = outerWallSurfaceSmoothed
    
    
    if not centerlinesImported:
        allDataSets['centerlines'] = centerlines
    
    imageExists = False
    if imageArrayName:
        try:
            imageArray = matlabData[imageArrayName]
            image = numpy_array_to_vtk_image(imageArray, imageType, spacing)
            image = pad_image_by_one(image)
            allDataSets['image'] = image
            imageExists = True
        except NameError:
            print(f'No image array named {imageArrayName}')
    
    # add inner segmentation array to image if it exists
    if imageExists and (imageArray.shape == innerWallSeg.shape) and \
        (imageArray.shape == outerWallSeg.shape):
        image.GetPointData().AddArray(innerWallSegImage.GetPointData().GetScalars()) 
        image.GetPointData().AddArray(outerWallSegImage.GetPointData().GetScalars())
        
        if imageSmoothing:
            innerWallSegSmoothedVTKArray = innerWallSegImageSmoothed.GetPointData().GetScalars()
            innerWallSegSmoothedVTKArray.SetName('InnerWallSmoothed')
            image.GetPointData().AddArray(innerWallSegSmoothedVTKArray)
            outerWallSegSmoothedVTKArray = outerWallSegImageSmoothed.GetPointData().GetScalars()
            outerWallSegSmoothedVTKArray.SetName('OuterWallSmoothed')
            image.GetPointData().AddArray(outerWallSegSmoothedVTKArray)     
    else:
        allDataSets['inner_wall_segmentation'] = innerWallSegImage
        allDataSets['outer_wall_segmentation'] = outerWallSegImage
        if imageSmoothing:
            allDataSets['inner_wall_segmentation_smoothed'] = innerWallSegImageSmoothed
            allDataSets['outer_wall_segmentation_smoothed'] = outerWallSegImageSmoothed
    #%% save datasets
    basename, _ = os.path.splitext(matfilename)
    if useOuterWallForCenterlines:
        if suffix:
            optionalUnderscore = '_'
        else:
            optionalUnderscore = ''
        suffix = f'{suffix}{optionalUnderscore}outer-wall-derived'
    if suffix:
        basename = f'{basename}_{suffix}'
    
    for name, dataset in allDataSets.items():
        if isinstance(dataset, vtk.vtkImageData):
            ext = 'vti'
        elif isinstance(dataset, vtk.vtkPolyData):
            ext = 'vtp'
        filename = f'{basename}_{name}.{ext}'
        save_vtkxml(dataset, prepend_wd(filename))
        print(f"{name.capitalize().replace('_',' ')} saved.")