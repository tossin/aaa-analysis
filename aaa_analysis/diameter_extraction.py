# -*- coding: utf-8 -*-
"""
Created on Tue Oct  6 11:32:42 2020

@author: tossi
"""

import numpy as np

import vtk
from  vtk.numpy_interface import (dataset_adapter as dsa,
                                  algorithms as algs)

wdo = dsa.WrapDataObject

def get_section(inputData, pt, n):
    plane = vtk.vtkPlane()
    plane.SetOrigin(pt)
    plane.SetNormal(n)
    
    cutter = vtk.vtkCutter()
    cutter.SetInputData(inputData)
    cutter.SetCutFunction(plane)
    cutter.Update()
    
    conn = vtk.vtkPolyDataConnectivityFilter()
    conn.SetInputData(cutter.GetOutput())
    conn.SetExtractionModeToClosestPointRegion()
    conn.SetClosestPoint(pt)
    conn.Update()
    
    stripper = vtk.vtkStripper()
    stripper.SetInputData(conn.GetOutput())
    stripper.Update()
    return stripper.GetOutput()
    
def resample_line(line, numSubdivisions):
    splineFilter = vtk.vtkSplineFilter()
    splineFilter.SetInputData(line)
    splineFilter.SetSubdivideToSpecified()
    splineFilter.SetNumberOfSubdivisions(numSubdivisions)
    splineFilter.Update()
    return splineFilter.GetOutput()

def calculate_angle(unitdir, deg0dir, deg90dir):
    costheta = np.sum(unitdir * deg0dir, axis=1)
    sintheta = np.sum(unitdir * deg90dir, axis=1)
    sign = np.sign(sintheta)
    angle = np.rad2deg(np.arccos(costheta)) * sign + 180.0
    return angle        
    
def calculate_section_area(inputSection):
    
    section = vtk.vtkPolyData()
    section.DeepCopy(inputSection)
    
    sectionPolyline = section.GetCell(0)
    polylinePtIds = sectionPolyline.GetPointIds()
    idlist = vtk.vtkIdList()

    for i in range(polylinePtIds.GetNumberOfIds()):
        idlist.InsertNextId( polylinePtIds.GetId(i) )
        
    polysArray = vtk.vtkCellArray()
    polysArray.Allocate(1)         
    polysArray.InsertNextCell(idlist)
    
    section.SetPolys(polysArray)
    section.BuildCells()
    
    numCells = section.GetNumberOfCells()
    area = section.GetCell(numCells-1).ComputeArea()
    
    return area 

def find_nearest(array, value):
    nearestIdx = np.abs(array - value).argmin()
    return nearestIdx

class SurfaceMaxDiameterExtraction:
    
    def __init__(self, surface, centerline,
        normalsArrayName,
        zeroDegreeArrayName,
        startIndex=0, endIndex=0, interval=1,
        pointsPerSection=60,
        outputMaxDiameterLines=False):
        
        self.Surface = surface
        self.Centerline = centerline
        self.SurfaceCrossSections = None
        
        self.NormalsArrayName = normalsArrayName
        self.ZeroDegreeArrayName = zeroDegreeArrayName
        
        self.StartIndex = startIndex
        self.EndIndex = endIndex
        self.Interval = interval
        self.PointsPerSection = pointsPerSection
        self.OutputMaxDiameterLines = outputMaxDiameterLines
        
    def _GetSectionIndexes(self):
        numPoints = self.Centerline.GetNumberOfPoints()
        if self.EndIndex+1 > numPoints or self.EndIndex == 0:
            self.EndIndex = numPoints-1
        elif self.EndIndex < 0:
            self.EndIndex = numPoints + self.EndIndex
            
        self.SectionIndexes = np.arange(self.StartIndex, self.EndIndex+1,
                                        self.Interval).astype(np.int_)
    
    def _GetCrossSections(self):
        
        centerline_dsa = wdo(self.Centerline)
        #surface_dsa = wdo(self.Surface)
        
        normals = centerline_dsa.PointData[self.NormalsArrayName]
        deg0dirs = centerline_dsa.PointData[self.ZeroDegreeArrayName]
        deg90dirs = algs.cross(normals, deg0dirs)
        points = centerline_dsa.Points
        
        #numSections = self.SectionIndexes.size
        
        appender = vtk.vtkAppendPolyData()
        
        for sectionIndex in self.SectionIndexes:
            
            pt = points[sectionIndex,:]
            n = normals[sectionIndex,:]
            deg0dir = deg0dirs[sectionIndex,:]
            deg90dir = deg90dirs[sectionIndex,:]
            
            section = get_section(self.Surface, pt, n)
            section = resample_line(section, self.PointsPerSection)        
            section_dsa = dsa.WrapDataObject(section)            

            radiusVector = section_dsa.Points - pt
            radiusDir = algs.norm(radiusVector)
            radius = algs.mag(radiusVector)

            # The diameters are estimated by assigning each point an angle theta, 
            # finding the point whose angle is closest to theta + 180.0, and 
            # calculating the distance between those two points
            angle = calculate_angle(radiusDir, deg0dir, deg90dir)

            numPoints = section.GetNumberOfPoints()
            oppAngle = (angle + 180.0) % 360.0
            
            # Unfortunately numpy v1.8.1 has terrible broadcasting behavior, so we
            # will use a list comprehension. If Kitware upgrades the numpy version
            # to 1.11, use the commented out code instead.
            
            numpyMajorVer, numpyMinorVer, numpyPatchVer = [int(v) for v in np.__version__.split('.')]
            if numpyMajorVer > 1 or (numpyMajorVer == 1 and numpyMinorVer > 8):
                angleMatrix = np.repeat(angle[:,np.newaxis], angle.size, axis=1)
                oppAnglePtIds = np.argmin(angleMatrix - oppAngle, axis=1)
            else:
                oppAnglePtIds = [find_nearest(angle, oppAng) for oppAng in oppAngle]
                oppAnglePtIds = np.array(oppAnglePtIds, dtype=np.int_)
            oppAnglePts = section_dsa.Points[oppAnglePtIds,:]
                
            diameterVector = section_dsa.Points - oppAnglePts
            diameters = algs.mag(diameterVector)
            
            maxDiam = diameters.max()
            #maxDiameters[k] = maxDiam
            maxDiamRatio = diameters/maxDiam
            
            maxDiameterPtsIndicator = np.zeros(numPoints).astype(np.int_)
            maxDiamPtId1 = np.where(diameters==maxDiam)[0][0]
            maxDiamPtId2 = oppAnglePtIds[maxDiamPtId1]        
            maxDiameterPtsIndicator[[maxDiamPtId1,maxDiamPtId2]] = 1

            area = calculate_section_area(section)
            
            if self.OutputMaxDiameterLines:
                sectionLinesArray = section.GetLines()
            
                maxDiamLine = vtk.vtkLine()
                maxDiamLinePtIds = maxDiamLine.GetPointIds()
                maxDiamLinePtIds.SetId(0, maxDiamPtId1)
                maxDiamLinePtIds.SetId(1, maxDiamPtId2)
            
                sectionLinesArray.InsertNextCell(maxDiamLine)
                
                maxDiamArray = np.ones(2) * maxDiam
                cellSectionIdArray = np.ones(2) * sectionIndex
                sectionAreaArray = np.ones(2) * area
            else:
                maxDiamArray = np.atleast_1d(maxDiam)
                cellSectionIdArray = np.atleast_1d(sectionIndex)
                sectionAreaArray = np.atleast_1d(area)

            section_dsa.PointData.append(radiusVector, 'RadiusVector')
            section_dsa.PointData.append(radius, 'Radius')
            section_dsa.PointData.append(angle, 'Angle')
            section_dsa.PointData.append(oppAngle, 'OppositeAngle')
            section_dsa.PointData.append(oppAnglePtIds, 'OppositeAnglePointId')
            section_dsa.PointData.append(diameterVector, 'DiameterVector')
            section_dsa.PointData.append(diameters, 'Diameter')
            section_dsa.PointData.append(maxDiamRatio, 'DiameterRatioToSectionMax')
            section_dsa.PointData.append(maxDiameterPtsIndicator, 'MaxDiameterPtsIndicator')
            
            sectionIds = np.ones(numPoints) * sectionIndex
            section_dsa.PointData.append(sectionIds, 'SectionId')
            
            section_dsa.CellData.append(maxDiamArray, 'MaxDiameter')
            section_dsa.CellData.append(cellSectionIdArray, 'SectionId')
            section_dsa.CellData.append(sectionAreaArray, 'SectionArea')
        
            appender.AddInputData(section)        

        appender.Update()
        self.SurfaceCrossSections = appender.GetOutput()
        
    def __call__(self):
        
        self._GetSectionIndexes()
        self._GetCrossSections()
        
        return self.SurfaceCrossSections