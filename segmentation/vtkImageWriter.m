function vtkImageWriter(imageData, filepath, varargin) %spacing, origin, title

numvarargs = length(varargin);

dimensions = size(imageData)

opts = {[1,1,1],[0,0,0],'vtk image file exported from MATLAB'};

if numvarargs > 3
	opts(1:numvarargs) = varargin;
end

spacing = opts{1};
origin = opts{2};
title = opts{3};

fid = fopen(filepath, 'w+');

%% write header information
header = '# vtk DataFile Version 3.0\n';
datatype = 'ASCII\n';
topology = 'DATASET STRUCTURED_POINTS\n';

fprintf(fid, header);
fprintf(fid, [title '\n']);
fprintf(fid, datatype);
fprintf(fid, topology);

%% geometry
dimensions_spec = 'DIMENSIONS %d %d %d\n';
origin_spec = 'ORIGIN %f %f %f\n';
spacing_spec = 'SPACING %f %f %f\n';

Nx = dimensions(1);
Ny = dimensions(2);
Nz = dimensions(3);

fprintf(fid, dimensions_spec, Nx, Ny, Nz);
fprintf(fid, origin_spec, origin(1), origin(2), origin(3));
fprintf(fid, spacing_spec, spacing(1), spacing(2), spacing(3));

%% attribute data
numPoints = Nx*Ny*Nz;
pointdata = 'POINT_DATA %d\n';
scalars = 'SCALARS DICOMImage unsigned_long 1\n';
lookuptable = 'LOOKUP_TABLE default\n';

fprintf(fid, pointdata, numPoints);
fprintf(fid, scalars);
fprintf(fid, lookuptable);

counter = 0;
for i = 1:Nz
	for j = 1:Ny
		for k = 1:Nx
			counter = counter + 1;
			if mod(counter, 12) == 0
				fprintf(fid, '%d\n', imageData(k,j,i));
			else
				fprintf(fid, '%d ', imageData(k,j,i));
			end
		end
	end
end

fclose(fid);